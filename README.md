# Background
This repo contains bash scripts for creating ec2 instance, with all the needed configuratios.

## Requirements to run locally
In order to run the script from local, make sure you have:
```bash 
    aws-cli
```

## AWS credentials
Your creds can be found under `~/.aws/` directory. This directory, with this file, is automatically created once you have downloaded the aws-cli.
Make sure to define `[default]` above the `aws-secret-access-key` and `aws-access-key-id`. See [this link](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html) as an example for how to configure it.

**This script WORKS ONLY for region us-east-1.**

If you want it to work for other region, you need to change the `UBUNTU_20_04_AMI` variable's value in the `main.sh` file.

Alternatively, you can run from your terminal the command `aws configure`, and set your credentials there. 

## Delete instance
Don't forget to delete your instance once you are finished!
