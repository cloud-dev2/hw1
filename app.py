from datetime import datetime
from flask import Flask, request, jsonify

app = Flask(__name__)

entries = []
ticket_id = 0


@app.route("/entry", methods=["POST"])
def entry():
    global ticket_id
    parking_lot = request.args.get("parkingLot", "unknown")
    plate = request.args.get("plate")

    if not plate:
        return jsonify({"error": "No plate was detected"})

    for entry in entries:
        if entry["plate"] == plate:
            return jsonify({"error": "This car is already parking"})

    entries.append(
        {
            "ticket_id": ticket_id,
            "plate": plate,
            "parking_lot": parking_lot,
            "entry_time": datetime.now().isoformat(),
        }
    )

    ticket_id += 1
    return jsonify({"ticket_id": ticket_id - 1})


@app.route("/exit", methods=["POST"])
def exit():
    ticket_id = request.args.get("ticketId")

    try:
        ticket_id = int(ticket_id)
        for entry in entries:
            if entry["ticket_id"] == ticket_id:
                plate = entry["plate"]
                parking_lot = entry["parking_lot"]
                entry_time = datetime.fromisoformat(entry["entry_time"])
                entries.remove(entry)
                duration_in_minutes = round(
                    (datetime.now() - entry_time).total_seconds() / 60)
                cost = calc_cost(entry_time)
                return jsonify(
                    {
                        "plate": plate,
                        "parking_lot": parking_lot,
                        "total_parked_time": f"{duration_in_minutes} minutes",
                        "cost": cost,
                    }
                )
        return jsonify({"error": "Ticket ID wasnt found in the DB"})

    except:
        return jsonify({"error": "Invalid ticket ID - should be a number"})


def calc_cost(entry_time):
    exit_time = datetime.now()
    parked_time = exit_time - entry_time
    parked_time_minutes = parked_time.total_seconds() // 60
    parked_time_quarters = (parked_time_minutes) // 15
    return parked_time_quarters * 2.5  # 10 / 4


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
